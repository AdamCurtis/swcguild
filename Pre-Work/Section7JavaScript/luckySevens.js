function luckySevens(){

var startingBet = document.getElementById("startingBet").value;
var dieOne = 0;
var dieTwo = 0;
var dice = 0;
var totalRolls = 0;
var money = startingBet;
var moneyHeld = [startingBet];
var win = 4;
var loss = 1;

			do{
			totalRolls++;
			dieOne = Math.ceil(Math.random() * 6) + 1;
			dieTwo = Math.ceil(Math.random() * 6) + 1;	
			dice = dieOne+dieTwo;

			if (dice == 7){
			money = money+win;
			var last_element = moneyHeld[moneyHeld.length -1]; 
			var last_updated = last_element + win;
			moneyHeld.push(last_updated);

			}else {
			money--;
			var last_element = moneyHeld[moneyHeld.length -1];
			var last_updated = last_element - loss;
			moneyHeld.push(last_updated);
			totalRolls = totalRolls++;
			}

			}while(money>0);
			var totalMax = 0;
			totalMax = Math.max.apply(Math, moneyHeld);
			numRolls = moneyHeld.indexOf(totalMax);
			if(numRolls<1){
			numRolls = 0;
			}

	document.getElementById("resultsTable").innerHTML=("<br><br><table b><th><ins>Results</ins></th><tr><td>Starting Bet</td><td>$" + startingBet + ".00</td></tr><tr><td>Total Rolls Before<br>Going Broke</td><td>"+ totalRolls +"</td></tr><tr><td>Highest Amount Won</td><td>"+totalMax+"</td></tr><tr><td>Roll Count at Highest<br>Amount Won</td><td>"+ numRolls + "</td></tr></table></center>");
	document.getElementById("button").innerHTML="Play Again";
}